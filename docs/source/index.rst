.. CallersBaneAPI documentation master file, created by
   sphinx-quickstart on Wed Jun 27 18:13:34 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CallersBaneAPI's documentation!
==========================================

.. toctree::
   :maxdepth: 2

   readme
   modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
