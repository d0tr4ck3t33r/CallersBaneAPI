Callers Bane Client Side API
============================
This projects goal is it to document the Client side API of the game Callers Bane and to maintain a low level python3 implementation.

How to install
--------------
Just put the ``callers_bane_api.py`` file into your project folder.
There is no fancy ``pip`` package.

How to build the documentation
------------------------------
1. ``pip install sphinx``
2. ``cd docs``
3. ``make html``

How to use
----------
Please follow the `API Documentation <https://d0tr4ck3t33r.gitlab.io/CallersBaneAPI>`_ for that.

How to contribute
-----------------
Contributions are always welcome.
There are a few ways to contribute, i.e.

1. Reporting bugs
2. Contributing to the documentation
3. Helping to reverse engineer the API

Disclaimer
----------
I don't take any liability for damage caused by this software.

Licence
-------
BSD-3