import socket
import json
import time
import hashlib
import os


def recv_timeout(the_socket, timeout=2):
    the_socket.setblocking(0)

    total_data = []
    data = ''

    begin = time.time()
    while 1:
        if total_data and time.time() - begin > timeout:
            break

        elif time.time() - begin > timeout * 2:
            break

        try:
            data = the_socket.recv(8192)
            if data:
                total_data.append(data)
                begin = time.time()
            else:
                time.sleep(0.1)
        except:
            pass

    return list(map(lambda b: b.decode(), total_data))


class CallersBaneAPI(object):
    """
    This Class is used to connect to a Callers Bane Game Server and will hopefully have all methods the client can use
    at some point.
    The API starts a TCP session on the Game Server Port and communicates via sending JSON objects to the server and
    getting answer(s) from the server in form of JSON objects
    There is no encryption used.

    The server architecture has three different types of nodes:

    lookup: Load balancing (choosing to which lobby node to connect).

    lobby: Managing decks, store, etc.

    match: Server for active matches.

    Each type can have a different amount of nodes. In the community server there is only one active lobby node.
    """

    def __init__(self, uri, port):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect((uri, port))

    def __del__(self):
        self.s.close()

    def __send(self, msg_obj):
        msg_json = json.dumps(msg_obj)
        self.s.sendall(msg_json.encode())

    def __recv(self):
        recvd_msgs = recv_timeout(self.s)

        msg_list = ''.join(recvd_msgs).split('\n\n')

        msg_json_list = []
        for m in msg_list:
            try:
                msg_json_list.append(json.loads(m))
            except:
                pass
        return msg_json_list

    def __get_msg(self, ctype):
        return filter(lambda r: r.get('msg') == ctype, self.__recv()).__next__()

    #################################
    ### API                       ###
    #################################

    def login(self, email, password, auth_str=None):
        """
        Method for login into the specified Server
        This needs to be called before you can do anything else

        Args:
            email: Your Username

            password: Your password as a RAW STR

            auth_str: Just some string. Use unknown atm

        Returns:
             List:
                [{'msg': 'ServerInfo, 'version': str, 'assetURL': str, 'newsURL': str, 'roles': str},
                {'msg': 'MappedStrings', 'strings': [{'key': str, 'value' str},...]}
                {'msg': 'CardTypes', 'cardTypes': [{'id': uint, 'name':  str, 'description': str, 'flavor': str,
                'subTypesStr': str, 'subTypes': [str,...], 'kind': str, 'rarity': uint, 'hp': uint, 'ap': unit, 'ac': uint,
                'costDecay': unit, 'costOrder': unit, 'costGrowth': unit, 'costEnergy': unit, 'set': unit, ,
                'limitedWeight': float, 'tags': {'sound_attack': str}, 'cardImage': unit, 'animationPreviewImage': unit,
                'animationPreviewInfo': 'float,float,float', 'animationBundle': unit,
                'abilities': [{'id': 'Move', 'name': 'Move', 'description': 'Move unit to adjacent tile',
                'cost': {'DECAY': 0, 'ORDER': 0, 'ENERGY': 0, 'GROWTH': 0, 'SPECIAL': 0}}], 'targetArea': 'FORWARD',
                'passiveRules': [{'displayName': 'Ranged attack', 'description': 'This unit is unaffected by effe...'}],
                'available': bool}
                {'msg': 'AvatarTypes', 'types': [{'id': unit, 'type': str, 'part': str, 'filename': str, 'set': str},...]}
                {'msg': 'IdolTypes', 'types' [{id: uint, 'type': str, 'name': str, 'filename': str},...]}
                {'msg': 'AchievementTypes', 'achievementTypes': [{'id': unit, 'name': str , 'description': str,
                'goldReward': unit, 'group': unit, 'sortId': uint, 'icon': str},...]}
                {'msg': 'ProfileInfo',
                'profile': {'id': unit, 'name': str, 'adminRole': str, 'featureType': str, 'isParentalConsentNeeded': bool},
                'userUuid': str, 'profileUuid': str}
                {'msg': 'ServerSettings', 'friendsListEnabled': bool, 'shardsEnabled': bool, 'mpQuickMatchEnabled': bool}
                {'msg': 'Connect', 'op': 'FirstConnect'}
                {'msg': 'News', 'news': str}
                {'msg': 'ActiveGame'}
        """
        # The secret salt used by the client
        best_salt_ever = r'ScrollsClientSalt5438_'
        # The password is hashed with sha256
        # This created some problems because C# and Python are using different strings. Raw Strings + UTF-16 seems to
        # work
        sha = hashlib.sha256()
        sha.update(str(best_salt_ever + password).encode('UTF-16LE'))
        hashed_password = sha.hexdigest()
        del sha
        # The authHash is just some string hashed. It doesn't seem to matter what the string contains (*hashed*)
        # The client uses some relative unique Id that Unity creates via some sys calls
        auth_hash = ''
        sha = hashlib.sha256()
        if auth_str is None:
            sha.update(os.name.encode())
            auth_hash = sha.hexdigest()
        else:
            sha.update(auth_str.encode())
            auth_hash = sha.hexdigest()

        Login = {
            'email': email,
            'password': hashed_password,
            'authHash': auth_hash,
            'msg': "FirstConnect"
        }
        self.__send(Login)
        return self.__recv()

    def close(self):
        """
        Closes the TCP socket.
        """
        self.s.close()

    def ping(self):
        """
        Pings the server. Can be used to keep the connection to the server open.

        Returns:
            Dict:
                {'msg': 'Ping', 'time': timestamp}
        """
        ctype = "Ping"
        Ping = {
            'msg': ctype
        }
        self.__send(Ping)
        return self.__get_msg(ctype)

    def lobby_lookup(self):
        """
        It's used for load balancing, it will direct the client to a lobby server with the least amount of  people.
        WARNING:
        Can crash your session and/or break other commands, must be used with caution

        Returns:
            Dict:
                {'ip': str, 'port': uint, 'msg': 'LobbyLookup'}
        """
        ctype = "LobbyLookup"
        Lobby = {
            'msg': ctype
        }
        self.__send(Lobby)
        return self.__get_msg(ctype)

    def join_lobby(self):
        """
        Moves the client to the lobby scene.

        Returns:
        """
        ctype = "JoinLobby"
        JoinLobby = {
            'msg': ctype
        }
        self.__send(JoinLobby)
        return self.__recv()

    def join_match(self):
        pass

    def lobby_library_view(self, profile_id=0):
        """
        Get list of every card you own

        Args:
            profile_id: Purpose still unknown FIXME

        Returns:
            Dict:
                {'msg': 'LibraryView', 'profileId': uint, 'cards': [{'id': uint, 'typeId': uint, 'tradable": bool,
                'isToken': bool, 'level': uint},...]}
        """
        ctype = "LibraryView"
        DeckList = {
            'msg': ctype,
            'profileId': profile_id
        }
        self.__send(DeckList)
        return self.__get_msg(ctype)

    def lobby_deck_list(self):
        """
        Returns all decks you have saved on your Account.

        Returns:
            Dict:
                {'msg': 'DeckList', 'decks': [{'name': str, 'resources': str, 'valid': bool, 'updated': str,
                'timestamp': timestamp}
        """
        ctype = "DeckList"
        DeckList = {
            'msg': ctype
        }
        self.__send(DeckList)
        return self.__get_msg(ctype)

    def lobby_deck_load(self, deck_name):
        """
        Load saved deck recipe

        Args:
            deck_name: Name of deck (From the list you can get via the deck_list method)

        Returns:
            Dict:
                {'deck': deck_name, 'cards': [{'id': uint, 'typeId': uint, 'tradable': bool, 'isToken': bool, 'level': uint},...],
                'resources': [str,...], 'valid': bool}
        """
        ctype = "DeckCards"
        DeckList = {
            'msg': ctype,
            'deck': deck_name
        }
        self.__send(DeckList)
        return self.__get_msg(ctype)

    def lobby_deck_save(self, deck_name, cards, metadata=None):
        """
        This method is used for saving a deck.

        Args:
            deck_name:  Name of the Deck (str)
            cards: List of card IDs (str)
            metadata: {'pos': "<cardID>,<x>,<y>|<cardID>,<x>,<y>|...."} x & y: int < 1000

        Returns:
        """
        if metadata is None:
            metadata = {'pos:': ''.join(map(lambda id: id + ',0,959|', cards))}
        ctype = "DeckSave"
        Save = {
            'msg': ctype,
            'name': deck_name,
            'cards': cards,
            'metadata': metadata
        }
        self.__send(Save)
        return self.__recv()

    def lobby_deck_validate(self, cards):
        """
        This method checks if a deck is valid. Probably wise to use this before saving the deck.

        Args:
            cards: The cards that are in the deck. List of card IDs (str)

        Returns:
        """
        ctype = "DeckValidate"
        Validate = {
            'msg': ctype,
            'cards': cards,
        }
        self.__send(Validate)
        return self.__recv()

    def lobby_get_overall_stats(self):
        """
        ServerName, Logins last 24h, Active Logins, top Ranked Users, Weekly Winners

        Returns:
            Dict:
                {'serverName': str, 'loginsLast24h': uint, 'topRanked': [{'name': str, 'rating': uint},
                {'name': str, 'rating': uint, <'gameId': uint>},...], 'nrOfProfiles': uint, topLabEntries: [],
                'weeklyWinners': [{'profileId': uint, 'winType': str, 'userName':  str, <'gameId': uint>}
        """
        ctype = "OverallStats"
        Stats = {
            'msg': ctype,
        }
        self.__send(Stats)
        return self.__get_msg(ctype)

    def lobby_get_twitter_feed(self):
        """
        Returns statuses from connected Twitter Feed (probably empty list)

        Returns:
            Dict:
                {'msg': 'GetTwitterFeed', 'feed': {'statuses': []}}
        """
        ctype = "GetTwitterFeed"
        TwitterFeed = {
            'msg': ctype
        }
        self.__send(TwitterFeed)
        return self.__get_msg(ctype)

    def lobby_get_hint(self, id=0):
        """
        Get one hint aka "Did you know?"

        Args:
            id: Get a specific hint

        Returns:
            Dict:
                {'msg': 'DidYouKnow', 'id': uint, 'hint': str}
        """
        ctype = 'DidYouKnow'
        DidYouKnow = {
            'msg': ctype
        }
        self.__send(DidYouKnow)
        return self.__get_msg(ctype)

    def lobby_upgrade_cards(self, cards):
        """
        Upgrade three cards to one of the higher tier

        Args:
            cards: List with tree card ID's (uint)

        Returns:

        """
        ctype = "UpgradeCard"
        UpgradeCard = {
            'cardIds': cards,
            'msg': ctype
        }
        self.__send(UpgradeCard)
        return self.__get_msg(ctype)

    def lobby_store_items(self):
        """
        Get a list of the items that you can buy in the store

        Returns:
             Dict:
                {'items': [{'isPurchased': bool, 'avatarPart': uint, 'itemId': uint, 'costGold': uint, 'isPublic': bool,
                'description': str, 'expires': str, 'itemType': 'AVATAR', 'itemName': str},
                {'isPurchased': bool, 'avatar': {'id': uint, 'name': str, 'type': 'STORE', 'image': str,
                'description': str, 'head': uint, 'body': uint, 'leg': uint, 'armBack': uint, 'armFront': uint},
                'itemId': uint, 'costGold': uint, 'isPublic': bool, 'description': str, 'expires': str,
                'itemType': 'AVATAR_OUTFIT', 'itemName': str},
                {'deckName': str, 'cardTypeIds': [uint, uint, ...], 'deckDescription': str, 'isPurchased': bool,
                'itemId': uint, 'costGold': uint, 'isPublic': bool, 'description': str, 'itemType': 'DECK',
                'itemName': 'Deck'},
                {'itemId': uint, 'costGold': uint, 'isPublic': bool, 'description': str, 'itemType': 'CARD_FACE_DOWN',
                'itemName': 'Random Scroll'},
                {'idolId': uint, 'isPurchased': bool, 'itemId': uint, 'costGold': uint, 'isPublic': bool,
                'description': str, 'expires': str, 'itemType': 'IDOL', 'itemName': 'Idol'},
        """
        ctype = "GetStoreItems"
        StoreItems = {
            'msg': ctype
        }
        self.__send(StoreItems)
        return self.__get_msg(ctype)

    def lobby_store_item_buy(self, itemId, payWithShards=False):
        """
        Buy item from store

        Args:
            itemId:
                From lobby_store_items
            payWithShards:
                Mojang deactived shards, so always false

        Returns:
            Dict:
                {"cards":[{"id":uint,"typeId":uint,"tradable":bool,"isToken":bool,"level":uint}],"deckInfos":[],
                "avatars":[],"avatarParts":[],"idols":[],"msg":"BuyStoreItemResponse"}
        """
        ctype = "BuyStoreItem"

        BuyItem = {
            "itemId": itemId,
            "payWithShards": payWithShards,
            "msg": ctype,
        }
        self.__send(BuyItem)
        return self.__get_msg("BuyStoreItemResponse")

    def lobby_blackmarket_list_offers(self):
        """
        Lists all cards with prices that are on the blackmarket

        Returns:
             Dict:
                [{'available': [{'price': uint, 'level': uint, 'type': uint}, ...],
                'msg': 'MarketplaceAvailableOffersListView'}]
        """
        ctype = "MarketplaceAvailableOffersListView"
        BlackmarketOffers = {
            'msg': ctype
        }
        self.__send(BlackmarketOffers)
        return self.__get_msg(ctype)

    def lobby_blackmarket_offers(self):
        """

        Returns:
            Dict:
                {'offers': [], 'profileId': uint, 'maxNumOffers': uint, 'msg': 'MarketplaceOffersView'}
        """
        ctype = "MarketplaceOffersView"
        BlackmarketOffers = {
            'msg': ctype
        }
        self.__send(BlackmarketOffers)
        return self.__get_msg(ctype)

    def lobby_blackmarket_sold_list(self):
        """

        Returns:
            Dict:
                {'sold': [{'transactionId': uint, 'cardType': uint, 'level': uint, 'sellPrice': uint, 'fee': uint,
                'claimed': bool, 'cardId': uint}], 'msg': 'MarketplaceSoldListView'}
        """
        ctype = "MarketplaceSoldListView"
        Blackmarket = {
            'msg': ctype
        }
        self.__send(Blackmarket)
        return self.__get_msg(ctype)

    def lobby_blackmarket_make_deal(self, offerId):
        """
        Buy a scroll from the blackmarket

        Args:
             offerId:
                transactionId you get from the blackmarket_sold_list

        Returns:
            Dict:
                {"profileData":{"gold":uint,"starterDeckVersion":1,"spectatePermission":"ALLOW_CHAT","acceptTrades":bool,
                "acceptChallenges":bool,"rating":uint},"msg":"ProfileDataInfo"}
        """
        ctype = "MarketplaceMakeDeal"
        Deal = {
            'offerId': offerId,
            'msg': ctype
        }
        self.__send(Deal)
        return self.__get_msg("ProfileDataInfo")

    def lobby_blackmarket_create_offer(self, cardId, price):
        """
        Sell item on blackmarket

        Args:
            cardId:
                Card you want to sell
            price:
                Price you want to sell card for

        Returns:
             Dict:
                {}
        """
        ctype = "MarketplaceCreateOffer"
        CreateOffer = {
            'cardId': cardId,
            'price': price,
            'msg': ctype
        }
        self.__send(CreateOffer)
        return self.__recv()

    def lobby_blackmarket_offer_info(self, cardTypeId, cardLevel):
        """
        Get price info for card you want to sell
        Args:
            cardTypeId:
                Card type you want to know the price
            cardLevel:
                Level of the card

        Returns:
            Dict:
                {}
        """
        ctype = "MarketplaceCreateOfferInfo"
        OfferInfo = {
            'cardTypeId': cardTypeId,
            'cardLevel': cardLevel,
            'msg': ctype
        }
        self.__send(OfferInfo)
        return self.__recv()

    def lobby_check_card_dependencies(self, cards):
        ctype = "CheckCardDependencies"
        CardDependencies = {
            'cardIds': cards,
            'msg': ctype
        }
        self.__send(CardDependencies)
        return self.__get_msg(ctype)
